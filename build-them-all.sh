#!/usr/bin/env bash

# paru -Syu aurutils
# sudo mkdir -p /home/custompkgs
# sudo chown ${USER} /home/custompkgs
# repo-add /home/custompkgs/custom.db.tar.gz
# sudo sed 's,^#\[custom\],[custom],' -i /etc/pacman.conf
# sudo sed 's,^#SigLevel = Optional TrustAll,SigLevel = Optional TrustAll,' -i /etc/pacman.conf
# sudo sed 's,^#Server = file:///home/custompkgs,Server = file:///home/custompkgs ,' -i /etc/pacman.conf
# TODO: employ sane build order

_open="${1}"
_branch="${2}"

git pull
git clean -ffxd
if test "${1}" = "open"; then
	find . -name PKGBUILD -exec sh -c 'sed "/-m pyc_wheel/c\  #python -m pyc_wheel dist/*.whl" --in-place "{}"' \;
else
	find . -name PKGBUILD -exec sh -c 'sed "/-m pyc_wheel/c\  python -m pyc_wheel dist/*.whl" --in-place "{}"' \;
fi

for _pkgbuild in $(find . -name PKGBUILD); do
	if test -n "${2}"; then
		_substring="centralcontrol"
		if test "${_pkgbuild#*"${_substring}"}" != "${_pkgbuild}"; then
			sed "s|#branch=.*|#branch=${_branch}\")|" --in-place "${_pkgbuild}"
		fi
	else
		sed "s|#branch=.*|#branch=main\")|" --in-place "${_pkgbuild}"
	fi
done

find -name PKGBUILD -execdir sh -c 'makepkg --printsrcinfo > .SRCINFO' \;
# needs aurutils
aur graph */.SRCINFO | tsort | tac > /tmp/tehqueue # Remove unwanted targets
aur build --force --ignorearch --arg-file /tmp/tehqueue
rm /tmp/tehqueue
echo "Now you can install them all with:"
echo "sudo pacman -Syu $(find -maxdepth 1 -name "[!.]*" -type d -printf "%f ")"
