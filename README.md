# pkgbuilds

if mqtt_saver isn't building, set it's pkgver=1

## Workflow
from `man aur`

```
       Append a section for the local repository to /etc/pacman.conf

           [custom]
           SigLevel = Optional TrustAll
           Server = file:///home/custompkgs

       Create the repository root and database:

           $ sudo install -d /home/custompkgs -o $USER
           $ repo-add /home/custompkgs/custom.db.tar.gz

       If built packages are available, add them to the database:

           $ cd /home/custompkgs
           $ repo-add -n custom.db.tar.gz *.pkg.tar*

       Synchronize pacman:

           $ sudo pacman -Syu
```


``` bash
git clone https://www.gitlab.com/greyltc/pkgbuilds
cd pkgbuilds
find -name PKGBUILD -execdir sh -c 'makepkg --printsrcinfo > .SRCINFO' \;
aur graph */.SRCINFO | tsort | tac > queue # Remove unwanted targets
aur build -a queue
```
